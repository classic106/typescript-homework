import { initButtons } from './initButtons';
import { movieCards } from './movieCard';
import { renderRandomMovieBanner } from './renderRandomMovieBanner';

export async function render(): Promise<void> {
    // TODO render your app here
    movieCards;
    renderRandomMovieBanner();
    initButtons.initButtons();
}
