import { IMoviesResponse, IMovieInfResponse, IMovieInf } from './interfaces';
import { renderFavouritesMovies } from './renderFavoritesMovies';
import { getLocalStorage, setLocalStorage } from './helpers';

interface IMovieCards {
    fillCard: (item: IMovieInfResponse | IMovieInf) => HTMLDivElement | null;
    fillDataInCards: (data: IMoviesResponse) => HTMLDivElement | null;
}

class MovieCards implements IMovieCards {
    private card: Element | null = null;

    constructor() {
        const card = <Element>document.querySelector('.card');
        this.card = card;
    }

    public fillCard(item: IMovieInfResponse | IMovieInf, favorite?: boolean) {
        if (this.card) {
            const localStore = getLocalStorage('favorites');

            const newCardWrap = document.createElement('div');
            newCardWrap.setAttribute(
                'class',
                favorite ? 'col-12 p-2' : 'col-lg-3 col-md-4 col-12 p-2'
            );

            const newCard = this.card.cloneNode(true) as Element;

            newCard
                .querySelector('img')
                ?.setAttribute(
                    'src',
                    `https://image.tmdb.org/t/p/original/${item.poster_path}`
                );

            const svgElement = newCard.querySelector('svg');

            if (svgElement) {
                if (localStore.includes(item.id)) {
                    svgElement.setAttribute('fill', 'red');
                } else {
                    svgElement.setAttribute('fill', '#ff000078');
                }
            }

            if (svgElement) {
                svgElement.addEventListener('click', () => {
                    const fill = svgElement.getAttribute('fill');
                    const localStore = getLocalStorage('favorites');

                    if (fill === 'red') {
                        svgElement.setAttribute('fill', '#ff000078');

                        const newLocalStore = localStore.filter(
                            (val) => val !== item.id
                        );

                        renderFavouritesMovies(newLocalStore);
                        setLocalStorage('favorites', newLocalStore);
                    } else {
                        svgElement.setAttribute('fill', 'red');

                        const newLocalStore = [...localStore];
                        newLocalStore.push(item.id);
                        renderFavouritesMovies(newLocalStore);
                        setLocalStorage('favorites', newLocalStore);
                    }
                });
            }

            const cardText = newCard.querySelector(
                '.card-text'
            ) as HTMLParagraphElement;

            if (cardText) {
                cardText.innerText = item.overview;
            }

            const textMuted = newCard.querySelector(
                '.text-muted'
            ) as HTMLSpanElement;

            if (textMuted) {
                textMuted.innerText = item.release_date;
            }

            newCardWrap.appendChild(newCard);
            return newCardWrap;
        }
        return null;
    }

    public fillDataInCards(data: IMoviesResponse) {
        if (this.card) {
            const { results } = data;

            const filmContainer = document.createElement('div');
            filmContainer.setAttribute('id', 'film-container');
            filmContainer.setAttribute('class', 'row');

            results.map((item) => {
                const newCardWrap = this.fillCard(item);
                if (newCardWrap) {
                    filmContainer.appendChild(newCardWrap);
                }
            });

            return filmContainer;
        }
        return null;
    }
}

const movieCards = new MovieCards();

export { movieCards };
