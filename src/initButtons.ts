import { movies } from './movies';
import { renderFilmCards } from './renderFilmCards';

interface IInitButtons {
    initButtons: () => void;
}

class InitButtons implements IInitButtons {
    private btnGroupChoise = 'popular';

    private popularInput() {
        const popularInput = <HTMLButtonElement>(
            document.querySelector('#popular')
        );
        popularInput.addEventListener('change', () => {
            this.btnGroupChoise = 'popular';
            movies.setPageDefault();
            renderFilmCards.renderPopular();
        });
    }

    private upcomingInput() {
        const upcomingInput = <HTMLButtonElement>(
            document.querySelector('#upcoming')
        );
        upcomingInput.addEventListener('change', () => {
            this.btnGroupChoise = 'upcoming';
            movies.setPageDefault();
            renderFilmCards.renderUpcoming();
        });
    }

    private topRatedInput() {
        const topRatedInput = <HTMLButtonElement>(
            document.querySelector('#top_rated')
        );
        topRatedInput.addEventListener('change', () => {
            this.btnGroupChoise = 'top_rated';
            movies.setPageDefault();
            renderFilmCards.renderTopRetad();
        });
    }

    private initLoadMoreButton() {
        const loadMoreButton = <HTMLButtonElement>(
            document.querySelector('#load-more')
        );

        loadMoreButton.addEventListener('click', async () => {
            switch (this.btnGroupChoise) {
                case 'popular':
                    renderFilmCards.renderPopular();
                    break;
                case 'upcoming':
                    renderFilmCards.renderUpcoming();
                    break;
                case 'top_rated':
                    renderFilmCards.renderTopRetad();
                    break;
                case 'search':
                    renderFilmCards.renderSearchMovies();
                    break;
                default:
                    break;
            }
        });
    }

    private initSearchButton() {
        const searchButton = <HTMLButtonElement>(
            document.querySelector('#submit')
        );
        const searchInput = <HTMLInputElement>document.querySelector('#search');

        searchButton.addEventListener('click', async () => {
            const { value } = searchInput;

            this.btnGroupChoise = 'search';

            if (value) {
                movies.setPageSearchValue(value);
                movies.setPageDefault();
                renderFilmCards.renderSearchMovies();
            }
        });
    }

    public initButtons() {
        this.popularInput();
        this.topRatedInput();
        this.upcomingInput();
        this.initLoadMoreButton();
        this.initSearchButton();
    }
}

const initButtons = new InitButtons();

export { initButtons };
