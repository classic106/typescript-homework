import { api } from './api';
import { movieCards } from './movieCard';

async function renderFavouritesMovies(arr: number[]): Promise<void> {
    const { getMovie } = api;

    const res = await Promise.all(arr.map(async (val) => await getMovie(val)));

    const favoriteMovies = document.querySelector('#favorite-movies');

    if (favoriteMovies) {
        favoriteMovies.innerHTML = '';

        res.map((val) => {
            const card = movieCards.fillCard(val, true);
            if (card) {
                favoriteMovies?.appendChild(card);
            }
        });
    }
}

export { renderFavouritesMovies };
