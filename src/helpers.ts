import { IMovieInf, IMovieInfResponse } from './interfaces';

function getLocalStorage(val: string): number[] {
    const timerArr = localStorage.getItem(val);

    if (!timerArr || timerArr === 'undefined') return [];
    return JSON.parse(timerArr);
}

function setLocalStorage(val: string, arr: number[]): void {
    arr
        ? localStorage.setItem(val, JSON.stringify(arr))
        : localStorage.removeItem(val);
}

function randomInteger(min: number, max: number): number {
    return Math.floor(min + Math.random() * (max - min));
}

const getWhatNeed = (data: IMovieInfResponse[] | IMovieInf[]): IMovieInf[] => {
    return data.map((item) => {
        const newObj: IMovieInf = {
            id: 0,
            overview: '',
            poster_path: '',
            release_date: '',
            original_title: '',
        };

        newObj.id = item.id;
        newObj.overview = item.overview;
        newObj.poster_path = item.poster_path;
        newObj.release_date = item.release_date;
        newObj.original_title = item.original_title;

        return newObj;
    });
};

export { getWhatNeed, randomInteger, getLocalStorage, setLocalStorage };
