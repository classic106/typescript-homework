import { IMovieInf } from './interfaces';
import { api } from './api';

class Movies {
    private total_pages = 2;
    private searchValue = '';

    public movies: IMovieInf[] | null = null;
    public page = 1;

    public setPageDefault() {
        this.page = 1;
    }

    public setPageSearchValue(value: string) {
        this.searchValue = value;
    }

    async popularMoviews() {
        if (this.page < this.total_pages) {
            const result = await api.loadPopular(this.page);
            this.page += 1;
            this.total_pages = result.total_pages;
            this.movies = result.results;

            return result;
        }
        return null;
    }

    async topRetadMoviews() {
        if (this.page < this.total_pages) {
            const result = await api.loadTopRetad(this.page);
            this.page += 1;
            this.total_pages = result.total_pages;
            this.movies = result.results;

            return result;
        }
        return null;
    }

    async upcomingMoviews() {
        if (this.page < this.total_pages) {
            const result = await api.loadUpcoming(this.page);
            this.page += 1;
            this.total_pages = result.total_pages;
            this.movies = result.results;

            return result;
        }
        return null;
    }

    async searchMoviews() {
        if (this.page < this.total_pages) {
            const result = await api.searchMovies(this.searchValue, this.page);
            this.page += 1;
            this.total_pages = result.total_pages;
            this.movies = result.results;

            return result;
        }
        return null;
    }
}

const movies = new Movies();

export { movies };
