import { IMovieInf } from './interfaces';
import { renderFilmCards } from './renderFilmCards';
import { randomInteger } from './helpers';

async function renderRandomMovieBanner(): Promise<void> {
    const data = await renderFilmCards.renderPopular();

    let randomMovie: IMovieInf | null = null;

    if (data) {
        const { results } = data;
        const index = randomInteger(0, results.length);
        randomMovie = results[index];
    }

    const randomMovieName: HTMLHeadingElement | null =
        document.querySelector('#random-movie-name');
    const randomMovieDescription: HTMLParagraphElement | null =
        document.querySelector('#random-movie-description');

    if (randomMovie && randomMovieName && randomMovieDescription) {
        randomMovieName.innerText = randomMovie.original_title;
        randomMovieDescription.innerText = randomMovie.overview;
    }
}

export { renderRandomMovieBanner };
