import { IMoviesResponse } from './interfaces';
import { movies } from './movies';
import { movieCards } from './movieCard';

interface IRenderFilmCards {
    renderPopular: () => Promise<IMoviesResponse | null>;
    renderUpcoming: () => Promise<void>;
    renderTopRetad: () => Promise<void>;
    renderSearchMovies: (value: string) => Promise<void>;
}

class RenderFilmCards implements IRenderFilmCards {
    private updateContainer(data: IMoviesResponse | null) {
        if (data) {
            const filmContainer = movieCards.fillDataInCards(data);
            if (filmContainer && filmContainer.childNodes.length) {
                const container = document.getElementsByClassName(
                    'container'
                )[1] as Element;

                if (data.page === 1) {
                    container.innerHTML = '';
                    container.appendChild(filmContainer);
                } else {
                    container.appendChild(filmContainer);
                }
            }
        }
    }

    public async renderPopular() {
        const data = await movies.popularMoviews();
        this.updateContainer(data);
        return data;
    }

    public async renderUpcoming() {
        const data = await movies.upcomingMoviews();
        this.updateContainer(data);
    }

    public async renderTopRetad() {
        const data = await movies.topRetadMoviews();
        this.updateContainer(data);
    }

    public async renderSearchMovies() {
        const data = await movies.searchMoviews();
        this.updateContainer(data);
    }
}

const renderFilmCards = new RenderFilmCards();

export { renderFilmCards };
