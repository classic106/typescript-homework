interface IMovieInf {
    id: number;
    overview: string;
    poster_path: string;
    release_date: string;
    original_title: string;
}

interface IMovieInfResponse {
    adult: boolean;
    backdrop_path: string;
    genre_ids: number[];
    id: number;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path: string;
    release_date: string;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
}

interface IMoviesResponse {
    page: number;
    results: IMovieInfResponse[] | IMovieInf[];
    total_pages: number;
    total_results: number;
}

export { IMovieInf, IMovieInfResponse, IMoviesResponse };
