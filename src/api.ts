import axios, { AxiosResponse } from 'axios';
import { IMoviesResponse, IMovieInfResponse } from './interfaces';
import { my_key } from './myKey';
import { getWhatNeed } from './helpers';

interface IApi {
    loadUpcoming: (page: number) => Promise<IMoviesResponse>;
    loadTopRetad: (page: number) => Promise<IMoviesResponse>;
    loadPopular: (page: number) => Promise<IMoviesResponse>;
    searchMovies: (value: string, page: number) => Promise<IMoviesResponse>;
    getMovie: (id: number) => Promise<IMovieInfResponse>;
}

class Api implements IApi {
    public async loadUpcoming(page: number) {
        const result: AxiosResponse<IMoviesResponse> = await axios.get(
            `https://api.themoviedb.org/3/movie/upcoming?api_key=${my_key}&page=${page}`
        );

        const { data } = result;

        data.results = getWhatNeed(data.results);
        return data;
    }

    public async loadTopRetad(page: number) {
        const result: AxiosResponse<IMoviesResponse> = await axios.get(
            `https://api.themoviedb.org/3/movie/top_rated?api_key=${my_key}&page=${page}`
        );

        const { data } = result;

        data.results = getWhatNeed(data.results);
        return data;
    }

    public async loadPopular(page: number) {
        const result: AxiosResponse<IMoviesResponse> = await axios.get(
            `https://api.themoviedb.org/3/movie/popular?api_key=${my_key}&page=${page}`
        );

        const { data } = result;

        data.results = getWhatNeed(data.results);
        return data;
    }

    public async searchMovies(value: string, page: number) {
        const result: AxiosResponse<IMoviesResponse> = await axios.get(
            `https://api.themoviedb.org/3/search/movie?api_key=${my_key}&language=en-US&query=${value}&page=${page}&include_adult=false`
        );

        const { data } = result;

        data.results = getWhatNeed(data.results);
        return data;
    }

    public async getMovie(id: number) {
        const result: AxiosResponse<IMovieInfResponse> = await axios.get(
            `https://api.themoviedb.org/3/movie/${id}?api_key=${my_key}`
        );

        return result.data;
    }
}

const api = new Api();

export { api };
